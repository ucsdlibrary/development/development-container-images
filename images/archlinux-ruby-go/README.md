# Distrobox - Local Development

## Local Setup

Note, you can choose a `--name` and `--image` of your choice if you choose to `-t` the image differently

### Distrobox

```sh
docker build -t arch-toolbox-dev .
distrobox create --name arch-distrobox-dev --image arch-toolbox-dev
distrobox enter --name arch-distrobox-dev
```

## TODO
- Add a `contrib` directory with a `distrobox` [config file][distrobox-config]
- Get podman/docker mapped in via distrobox-host-exec as part of the image setup

[distrobox-config]:https://github.com/89luca89/distrobox?tab=readme-ov-file#configure-distrobox
