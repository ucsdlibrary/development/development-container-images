# Development Container Images

These are a set of container images that are intended to be used with tools such as:

- [distrobox][distrobox]
- [toolbox][distrobox]
- [vs code dev container][vs-code-devcontainer]

## Repository Organization

To add a new container image to the repository:

> Note: The directory name will be used to name/tag the image that is built and pushed to the container registry in Gitlab

### Create your container image

- Create a new directory named after your container image in `./images/. Example: `images/archlinux-ruby-go`,`ubuntu-python3`, etc.
- Create a `Dockerfile` in the directory, along with any additional files, binaries, etc. needed to build the image.
- Ensure the final build stage in the `Dockerfile` is named `production`
- Create a `README.md` in the container image directory with any specific/unique usage information a user needs to know.

### Create a build job for your image

- Open `.gitlab-ci.yml`
- Add a new build job using the directory name containing your new image. Let's use `ubuntu-python3` as an example.

```yaml
build:ubuntu-python3:
  extends:
    - .kaniko-build
  script:
    - build_image_for "ubuntu-python3"
```

### Using the built image

In the simplest case, you can pull the image down locally.

```sh
$ docker run --rm -it registry.gitlab.com/ucsdlibrary/development/development-container-images/archlinux-ruby-go:stable /bin/sh
```

For use in [distrobox][distrobox], for example, you can use the image via the `--image` flag like any other.

```sh
distrobox create --name arch-distrobox-dev --image registry.gitlab.com/ucsdlibrary/development/development-container-images/archlinux-ruby-go:stable
```

[distrobox]:https://github.com/89luca89/distrobox
[toolbox]:https://github.com/containers/toolbox
[vs-code-devcontainer]:https://code.visualstudio.com/docs/devcontainers/create-dev-container
